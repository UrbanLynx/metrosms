﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroSMS.Model.Entities;
using MetroSMS.Model.ModelWork;

namespace MetroSMS.Interface
{
    public partial class Form1 : Form
    {
        private const double TimeUnit = 60;

        public Form1()
        {
            InitializeComponent();
        }

        public ModelSettings InitializeSettings()
        {
            var modelSettings = new ModelSettings();

            modelSettings.Statistics.WorkingTime = Convert.ToDouble(textBox31.Text);

            var mxGeneratePassenger = TimeUnit/Convert.ToDouble(textBox1.Text);
            var dxGeneratePassenger = TimeUnit/Convert.ToDouble(textBox2.Text);

            modelSettings.PassengerSettings.GenerateNextPassengerTime = new TimeSettings(mxGeneratePassenger,dxGeneratePassenger);
            modelSettings.PassengerSettings.ProbabilityOfHavingTicket = Convert.ToDouble(textBox27.Text);

            for (int i = 0; i < Convert.ToInt32(textBox3.Text); i++)
            {
                modelSettings.TicketOfficesSettings.Add(new SimpleServingMachineSettings(new TimeSettings(Convert.ToDouble(textBox4.Text), Convert.ToDouble(textBox5.Text))));
            }

            for (int i = 0; i < Convert.ToInt32(textBox6.Text); i++)
            {
                modelSettings.TurnstileSettings.Add(new SimpleServingMachineSettings(new TimeSettings(Convert.ToDouble(textBox7.Text), Convert.ToDouble(textBox8.Text))));
            }

            for (int i = 0; i < Convert.ToInt32(textBox9.Text); i++)
            {
                modelSettings.EscalatorSettings.Add(new EscalatorSettings(Convert.ToDouble(textBox11.Text), Convert.ToDouble(textBox10.Text)));
            }

            modelSettings.TrainSettings.Add(new TrainSettings(Convert.ToDouble(textBox13.Text), Convert.ToDouble(textBox17.Text), Convert.ToInt32(textBox15.Text)));
            modelSettings.TrainSettings.Add(new TrainSettings(Convert.ToDouble(textBox18.Text), Convert.ToDouble(textBox14.Text), Convert.ToInt32(textBox16.Text)));

            return modelSettings;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            /*try
            {*/
                var modelSettings = InitializeSettings();
                ModelManager.Instance.Initialize(modelSettings);
                ModelManager.Instance.ModelWholeSystem();

                textBox23.Text = ModelManager.Instance.PassengerExistanceTime().ToString();
            /*}
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка при расчете модели. " + ex.Message);
            }*/
            
        }
    }
}
