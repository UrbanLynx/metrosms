﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MetroSMS.Model.ModelWork;

namespace MetroSMS.Model.Entities
{
    public class PassengerSettings
    {
        public TimeSettings GenerateNextPassengerTime { get; set; }
        public double ProbabilityOfHavingTicket { get; set; }
    }
}
