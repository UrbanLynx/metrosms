﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MetroSMS.Model.ModelWork;

namespace MetroSMS.Model.Entities
{
    public class SimpleServingMachineSettings
    {
        public SimpleServingMachineSettings() { }
        public SimpleServingMachineSettings(TimeSettings timeSettings)
        {
            ServiceTimeSettings = timeSettings;
        }
        public TimeSettings ServiceTimeSettings { get; set; }
    }
}
