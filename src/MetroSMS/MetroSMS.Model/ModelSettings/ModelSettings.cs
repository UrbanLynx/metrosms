﻿using System.Collections;
using System.Collections.Generic;
using MetroSMS.Model.Entities;

namespace MetroSMS.Model.ModelWork
{
    public class ModelSettings
    {
        public ModelSettings()
        {
            Statistics = new StatisticsSettings();
            PassengerSettings = new PassengerSettings();
            TicketOfficesSettings = new List<SimpleServingMachineSettings>();
            TurnstileSettings = new List<SimpleServingMachineSettings>();
            EscalatorSettings = new List<EscalatorSettings>();
            TrainSettings = new List<TrainSettings>();
        }
        
        public StatisticsSettings Statistics { get; set; }
        public PassengerSettings PassengerSettings { get; set; }
        public List<SimpleServingMachineSettings> TicketOfficesSettings { get; set; }
        public List<SimpleServingMachineSettings> TurnstileSettings { get; set; }
        public List<EscalatorSettings> EscalatorSettings { get; set; }
        public List<TrainSettings> TrainSettings { get; set; }
    }

    public class StatisticsSettings
    {
        public double WorkingTime { get; set; }
    }
}
