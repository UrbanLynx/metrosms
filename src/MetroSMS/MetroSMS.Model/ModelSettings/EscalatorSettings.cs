﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetroSMS.Model.Entities
{
    public class EscalatorSettings
    {
        public EscalatorSettings(double acceptanceTime, double rideTime)
        {
            AcceptanceTime = acceptanceTime;
            ServingTime = rideTime;
        }
        public double ServingTime { get; set; }
        public double AcceptanceTime { get; set; }
        
    }
}
