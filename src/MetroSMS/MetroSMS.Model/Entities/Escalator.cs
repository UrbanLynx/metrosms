﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MetroSMS.Model.Generator;
using MetroSMS.Model.ModelWork;

namespace MetroSMS.Model.Entities
{
    class Escalator:IServingMachine
    {
        private Queue<Passenger> _passengersQueue;
        private EscalatorSettings _escalatorSettings;
        private Action<Passenger> _callBackAfterServing;
        private bool _acceptanceInProgress;

        public Escalator(EscalatorSettings escalatorSettings, Action<Passenger> callBackAfterServing)
        {
            _escalatorSettings = escalatorSettings;
            _callBackAfterServing = callBackAfterServing;
            _passengersQueue = new Queue<Passenger>();
            _acceptanceInProgress = false;
        }

        public void PassengerIn(Passenger passenger)
        {
            if (_acceptanceInProgress)
            {
                _passengersQueue.Enqueue(passenger);
            }
            else
            {
                ServePassenger(passenger);
            }
        }

        public void PassengerOut(Passenger passenger)
        {
            _callBackAfterServing(passenger);
            ServePassenger();
        }

        public void StartServingMachine()
        {
            throw new NotImplementedException();
        }

        public int QueueLength
        {
            get { return _passengersQueue.Count; }
        }

        public void AcceptanceEnd(Passenger passenger)
        {
            _acceptanceInProgress = false;
            var servingTime = GenerateEndServingTime(); // примерно 40 сек - езда на эскалаторе
            var servingEvent = new ModelingEvent(() => PassengerOut(passenger), servingTime);
            ModelManager.Instance.AddModelingEvent(servingEvent);

            ServePassenger();
        }

        private void ServePassenger(Passenger passenger)
        {
            var acceptanceTime = GenerateEndAcceptanceTime(); // например 5 сек - зайти на эскалатор
            var acceptanceEvent = new ModelingEvent(() => AcceptanceEnd(passenger), acceptanceTime);
            ModelManager.Instance.AddModelingEvent(acceptanceEvent);
            _acceptanceInProgress = true;
        }

        private double GenerateEndAcceptanceTime()
        {
            return _escalatorSettings.AcceptanceTime;
        }

        private void ServePassenger()
        {
            if (_passengersQueue.Count > 0)
            {
                ServePassenger(_passengersQueue.Dequeue());
            }
        }

        private double GenerateEndServingTime()
        {
            return _escalatorSettings.ServingTime;
        }
    }
}
