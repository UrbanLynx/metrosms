﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetroSMS.Model.Entities
{
    public class Passenger
    {
        public bool HasTicket { get; set; }
        public double AppearTime { get; set; }
    }
}
